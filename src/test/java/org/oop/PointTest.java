package org.oop;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PointTest {
    @Test
    void twoPointsWithSameXAndYCoordinatesShouldHaveADistanceOfZero() {
        Point origin = new Point(0,0);
        double expectedDistance = 0;

        double actualDistance = Point.distance(origin, origin);

        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void distanceBetweenOriginAndPointsOnUnitCircleShouldBeOne() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(1, 0);
        Point point2 = new Point(0, 1);
        double expectedDistance = 1;
        double actualDistance;

        actualDistance = Point.distance(origin, point1);
        assertEquals(expectedDistance, actualDistance);

        actualDistance = Point.distance(origin, point2);
        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void distanceBetweenTwoOppositePointsOnUnitCircleShouldBeTwo() {
        Point point1 = new Point(1, 0);
        Point point2 = new Point(-1, 0);
        double expectedDistance = 2;
        double actualDistance;

        actualDistance = Point.distance(point1, point2);
        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void originAndPointOnPostiveXAxisShouldBeZeroRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(1, 0);
        Point point2 = new Point(3, 0);
        double expectedDistance = 0;
        double actualDistance;

        actualDistance = Point.direction(origin, point1);
        assertEquals(expectedDistance, actualDistance);

        actualDistance = Point.direction(origin, point2);
        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void originAndPointOnNegativeXAxisShouldBePiRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(-1, 0);
        Point point2 = new Point(-3, 0);
        double expectedDistance = Math.PI;
        double actualDistance;

        actualDistance = Point.direction(origin, point1);
        assertEquals(expectedDistance, actualDistance);

        actualDistance = Point.direction(origin, point2);
        assertEquals(expectedDistance, actualDistance);
    }

    @Test
    void originAndPointOnYAxisShouldBeHalfPiRadiansAway() {
        Point origin = new Point(0, 0);
        Point point1 = new Point(0, 1);
        Point point2 = new Point(0, 3);
        double expectedDistance = Math.PI / 2;
        double actualDistance;

        actualDistance = Point.direction(origin, point1);
        assertEquals(expectedDistance, actualDistance);

        actualDistance = Point.direction(origin, point2);
        assertEquals(expectedDistance, actualDistance);
    }
}
